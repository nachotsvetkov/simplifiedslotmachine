﻿using SimplifiedSlotMachine.BusinessLogic;
using SimplifiedSlotMachine.BusinessLogic.StateHandlers;
using SimplifiedSlotMachine.Models;
using SimplifiedSlotMachine.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Unity;

namespace SimplifiedSlotMachine
{
    public class StateMachineFactory
    {
        public static IStateMachine Get()
        {
            var container = new UnityContainer();

            container.RegisterSingleton<Player>();
            container.RegisterSingleton<Game>();
            container.RegisterType<IStateContext, StateContext>();
            container.RegisterType<IIOProvider, ConsoleIOProvider>();
            container.RegisterType<ICalculator, Calculator>();

            container.RegisterFactory<Slot[]>(c => new Slot[]
            {
                new Slot { DisplaySymbol = 'A', Coefficient = 0.4m, ShowRatePercentage = 45 },
                new Slot { DisplaySymbol = 'B', Coefficient = 0.6m, ShowRatePercentage = 35 },
                new Slot { DisplaySymbol = 'P', Coefficient = 0.8m, ShowRatePercentage = 15 },
                new Slot { DisplaySymbol = '*', Coefficient = 0.0m, ShowRatePercentage = 5, IsWildCard = true },
            });

            container.RegisterType<IRollEngine, RollEngine>();
            container.RegisterFactory<Dictionary<State, IStateHandler>>(c => new Dictionary<State, IStateHandler>
            {
                { State.Initial, new InitialStateHandler(c.Resolve<IStateContext>()) },
                { State.Balance, new BalanceStateHandler(c.Resolve<IStateContext>(), c.Resolve<IIOProvider>()) },
                { State.Stake, new StakeStateHandler(c.Resolve<IStateContext>(), c.Resolve<IIOProvider>()) },
                { State.Roll, new RollStateHandler(c.Resolve<IStateContext>(), c.Resolve<IRollEngine>()) },
                { State.DisplayMatrix, new DisplayMatrixStateHandler(c.Resolve<IStateContext>(), c.Resolve<IIOProvider>()) },
                { State.Calculate, new CalculateStateHandler(c.Resolve<IStateContext>(), c.Resolve<ICalculator>()) },
                { State.Pay, new PayStateHandler(c.Resolve<IStateContext>()) },
                { State.DisplayResult, new DisplayResultStateHandler(c.Resolve<IStateContext>(), c.Resolve<IIOProvider>()) },
                { State.End, new EndStateHandler(c.Resolve<IIOProvider>()) },
            });

            container.RegisterType<IStateMachine, StateMachine>();

            var stateMachine = container.Resolve<IStateMachine>();
            return stateMachine;
        }
    }
}
