﻿using SimplifiedSlotMachine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public static class PlayerExtensions
    {
        public static bool CanPlay(this Player player)
        {
            return player.Balance > 0;
        }
    }
}
