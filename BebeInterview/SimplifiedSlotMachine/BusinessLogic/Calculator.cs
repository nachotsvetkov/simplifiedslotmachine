﻿using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public class Calculator : ICalculator
    {
        public decimal GetWinningCoef(SlotMatrix matrix)
        {
            var totalWinCoef = 0.0m;
            for (int row = 0; row < matrix.Cells.GetLength(0); row++)
            {
                bool isWin = RowIsWin(matrix, row);

                if (!isWin)
                {
                    continue;
                }

                var rowWinCoef = matrix.Cells[row, 0].Coefficient + matrix.Cells[row, 1].Coefficient + matrix.Cells[row, 2].Coefficient;
                totalWinCoef += rowWinCoef;
            }

            return totalWinCoef;
        }

        public bool RowIsWin(SlotMatrix matrix, int row)
        {
            var firstAndSecondMatch = matrix.Cells[row, 0].DisplaySymbol == matrix.Cells[row, 1].DisplaySymbol
                || matrix.Cells[row, 0].IsWildCard
                || matrix.Cells[row, 1].IsWildCard;

            var firstAndThirdMatch = matrix.Cells[row, 0].DisplaySymbol == matrix.Cells[row, 2].DisplaySymbol
                || matrix.Cells[row, 0].IsWildCard
                || matrix.Cells[row, 2].IsWildCard;

            var secondAndThirdMatch = matrix.Cells[row, 1].DisplaySymbol == matrix.Cells[row, 2].DisplaySymbol
                || matrix.Cells[row, 1].IsWildCard
                || matrix.Cells[row, 2].IsWildCard;

            var isWin = firstAndSecondMatch && firstAndThirdMatch && secondAndThirdMatch;
            return isWin;
        }
    }
}
