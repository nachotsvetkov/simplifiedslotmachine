﻿using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public interface ICalculator
    {
        decimal GetWinningCoef(SlotMatrix matrix);
    }
}