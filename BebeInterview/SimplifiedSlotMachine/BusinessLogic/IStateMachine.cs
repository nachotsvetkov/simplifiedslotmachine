﻿namespace SimplifiedSlotMachine.BusinessLogic
{
    public interface IStateMachine
    {
        void Run();
    }
}