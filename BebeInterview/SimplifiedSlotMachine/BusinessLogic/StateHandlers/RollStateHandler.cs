﻿using System;
using System.Collections.Generic;
using System.Text;
using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class RollStateHandler : BaseStateHandler, IStateHandler
    {
        public RollStateHandler(IStateContext context, IRollEngine rollEngine) : base(context)
        {
            this.RollEngine = rollEngine;
        }

        public IRollEngine RollEngine { get; }

        public void Handle()
        {
            this.Context.Game.Matrix = this.RollEngine.Roll();
        }
    }
}
