﻿using SimplifiedSlotMachine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class BalanceStateHandler : BaseStateHandler, IStateHandler
    {
        public BalanceStateHandler(IStateContext context, IIOProvider inputProvider) : base(context)
        {
            this.InputProvider = inputProvider;
        }

        public IIOProvider InputProvider { get; }

        public void Handle()
        {
            decimal balance;
            do
            {
                balance = this.InputProvider.GetDecimal("Please deposit money you would like to play with:");
                
                if (balance <= 0)
                {
                    Console.WriteLine("Balance can not be less than or equal to 0");
                }
            } while (balance <= 0);


            this.Context.Player.Balance = balance;
        }
    }
}
