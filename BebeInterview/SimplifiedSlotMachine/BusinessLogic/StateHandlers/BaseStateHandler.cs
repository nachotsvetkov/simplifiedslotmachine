﻿using SimplifiedSlotMachine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class BaseStateHandler
    {
        public BaseStateHandler(IStateContext context)
        {
            this.Context = context;
        }

        public IStateContext Context { get; }
    }
}
