﻿using SimplifiedSlotMachine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class StakeStateHandler : BaseStateHandler, IStateHandler
    {
        public StakeStateHandler(IStateContext context, IIOProvider inputProvider) : base(context)
        {
            this.InputProvider = inputProvider;
        }

        public IIOProvider InputProvider { get; }

        public void Handle()
        {
            decimal stake;
            do
            {
                stake = this.InputProvider.GetDecimal("Enter stake ammount:");
                if (stake > this.Context.Player.Balance)
                {
                    Console.WriteLine("Stake is greater than balance! Please enter stake less than or equal to balance!");
                }

                if (stake <= 0)
                {
                    Console.WriteLine("Stake can not be less than or equal to 0");
                }
            } while (stake <= 0 || stake > this.Context.Player.Balance);
            this.Context.Game.Stake = stake;
        }
    }
}
