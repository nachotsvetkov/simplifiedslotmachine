﻿using System;
using System.Collections.Generic;
using System.Text;
using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class DisplayMatrixStateHandler : BaseStateHandler, IStateHandler
    {
        public DisplayMatrixStateHandler(IStateContext context, IIOProvider provider) : base(context)
        {
            this.IOProvider = provider;
        }

        public IIOProvider IOProvider { get; }

        public void Handle()
        {
            this.IOProvider.DisplayMatrix(this.Context.Game.Matrix);
        }
    }
}
