﻿using System;
using System.Collections.Generic;
using System.Text;
using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class DisplayResultStateHandler : BaseStateHandler, IStateHandler
    {
        public DisplayResultStateHandler(IStateContext context, IIOProvider provider) : base(context)
        {
            this.Provider = provider;
        }

        public IIOProvider Provider { get; }

        public void Handle()
        {
            this.Provider.DisplayResult(this.Context.Game.Winnings, this.Context.Player.Balance);
        }
    }
}
