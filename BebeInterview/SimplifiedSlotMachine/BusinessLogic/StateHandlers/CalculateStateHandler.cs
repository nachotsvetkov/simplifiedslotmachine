﻿using System;
using System.Collections.Generic;
using System.Text;
using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class CalculateStateHandler : BaseStateHandler, IStateHandler
    {
        public CalculateStateHandler(IStateContext context, ICalculator calculator) : base(context)
        {
            this.Calculator = calculator;
        }

        public ICalculator Calculator { get; }

        public void Handle()
        {
            var winCoeficient = this.Calculator.GetWinningCoef(this.Context.Game.Matrix);
            this.Context.Game.Winnings = winCoeficient * this.Context.Game.Stake;
        }
    }
}
