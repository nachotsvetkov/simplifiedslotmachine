﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class EndStateHandler : IStateHandler
    {
        public EndStateHandler(IIOProvider provider)
        {
            this.Provider = provider;
        }

        public IIOProvider Provider { get; }

        public void Handle()
        {
            this.Provider.DisplayEndOfGame();
        }
    }
}
