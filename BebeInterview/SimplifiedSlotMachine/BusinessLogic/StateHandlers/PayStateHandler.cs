﻿using System;
using System.Collections.Generic;
using System.Text;
using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class PayStateHandler : BaseStateHandler, IStateHandler
    {
        public PayStateHandler(IStateContext context) : base(context)
        {
        }

        public void Handle()
        {
            this.Context.Player.Balance += this.Context.Game.Winnings - this.Context.Game.Stake;
        }
    }
}
