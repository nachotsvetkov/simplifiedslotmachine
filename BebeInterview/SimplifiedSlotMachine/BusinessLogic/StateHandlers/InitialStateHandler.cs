﻿using SimplifiedSlotMachine.Models;
using SimplifiedSlotMachine.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic.StateHandlers
{
    public class InitialStateHandler : BaseStateHandler, IStateHandler
    {
        public InitialStateHandler(IStateContext context) : base(context)
        {
        }

        public void Handle()
        {
            this.Context.Player = new Player();
            this.Context.Game = new Game();
        }
    }
}
