﻿using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public interface IIOProvider
    {
        decimal GetDecimal(string message = "Enter Decimal Value:");

        void DisplayMatrix(SlotMatrix matrix);

        void DisplayResult(decimal winnings, decimal balance);

        void DisplayEndOfGame();
    }
}