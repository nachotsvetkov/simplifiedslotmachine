﻿namespace SimplifiedSlotMachine.BusinessLogic
{
    public interface IStateHandler
    {
        void Handle();
    }
}