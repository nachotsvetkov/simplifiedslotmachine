﻿using SimplifiedSlotMachine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public class RollEngine : IRollEngine
    {
        public Slot[] AvailableSlots { get; }

        public RollEngine(Slot[] availableSlots)
        {
            if (availableSlots.Length == 0)
            {
                throw new ArgumentException($"Slots are needed for the engine to work!");
            }

            var totalShowRatePercentage = availableSlots.Sum(s => s.ShowRatePercentage);
            if (totalShowRatePercentage != 100)
            {
                throw new ArgumentException($"Slot show rate is not 100%. It sums to {totalShowRatePercentage}. Check initialization...");
            }

            this.AvailableSlots = availableSlots;
        }

        public SlotMatrix Roll()
        {
            var matrix = new SlotMatrix();
            matrix.Cells = new Slot[4,3];

            for (int row = 0; row < 4; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    matrix.Cells[row, column] = GetSlot();
                }
            }

            return matrix;
        }

        public Slot GetSlot()
        {
            var random = new Random();
            var slotValue = random.Next(1, 101);

            var percentageRangeUpperBound = 0;
            for (int i = 0; i < AvailableSlots.Length; i++)
            {
                var currentSlot = AvailableSlots[i];
                percentageRangeUpperBound += currentSlot.ShowRatePercentage;
                if (slotValue <= percentageRangeUpperBound)
                {
                    return currentSlot;
                }
            }

            throw new Exception("Could not get slot whitin range! Check AvailableSlots!");
        }
    }
}
