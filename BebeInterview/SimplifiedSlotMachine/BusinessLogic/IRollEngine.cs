﻿using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public interface IRollEngine
    {
        public SlotMatrix Roll();
    }
}