﻿using System;
using System.Collections.Generic;
using System.Text;
using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public class ConsoleIOProvider : IIOProvider
    {
        public void DisplayEndOfGame()
        {
            Console.WriteLine("The Game have ended! Thanks for playing!");
        }

        public void DisplayMatrix(SlotMatrix matrix)
        {
            Console.WriteLine();
            for (int row = 0; row < 4; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    var slot = matrix.Cells[row, column];
                    Console.Write(slot.DisplaySymbol);
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public void DisplayResult(decimal winnings, decimal balance)
        {
            Console.WriteLine($"You have won: {winnings}");
            Console.WriteLine($"CurrentBalance is: {balance}");
        }

        public decimal GetDecimal(string message = "Enter Decimal Value:")
        {
            Console.WriteLine(message);

            decimal deposit;
            while (!decimal.TryParse(Console.ReadLine(), out deposit))
            {
                Console.WriteLine("Invalid value! Enter decimal: ");
            }

            return deposit;
        }
    }
}
