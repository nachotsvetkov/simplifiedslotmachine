﻿using SimplifiedSlotMachine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.BusinessLogic
{
    public class StateMachine : IStateMachine
    {
        public StateMachine(Dictionary<State, IStateHandler> stateHandlers, StateContext context)
        {
            this.StateHandlers = stateHandlers;
            this.Context = context;
        }

        public Dictionary<State, IStateHandler> StateHandlers { get; }

        public StateContext Context { get; }

        public void Run()
        {
            StateHandlers[State.Balance].Handle();

            while (Context.Player.CanPlay())
            {
                StateHandlers[State.Stake].Handle();
                StateHandlers[State.Roll].Handle();
                StateHandlers[State.DisplayMatrix].Handle();
                StateHandlers[State.Calculate].Handle();
                StateHandlers[State.Pay].Handle();
                StateHandlers[State.DisplayResult].Handle();
            }

            StateHandlers[State.End].Handle();
        }
    }
}
