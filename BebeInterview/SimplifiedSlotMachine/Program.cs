﻿namespace SimplifiedSlotMachine
{
    class Program
    {
        static void Main(string[] args)
        {
            var stateMachine = StateMachineFactory.Get();
            stateMachine.Run();
        }
    }
}
