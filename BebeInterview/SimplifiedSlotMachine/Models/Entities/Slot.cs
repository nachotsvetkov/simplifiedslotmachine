﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Models
{
    public class Slot
    {
        public char DisplaySymbol { get; set; }

        public decimal Coefficient { get; set; }

        public int ShowRatePercentage { get; set; }

        public bool IsWildCard { get; set; }
    }
}
