﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Models
{
    public class SlotMatrix
    {
        public Slot[,] Cells { get; set; }
    }
}
