﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Models.Entities
{
    public class Game
    {
        public SlotMatrix Matrix { get; set; }

        public decimal Stake { get; set; }

        public decimal Winnings { get; set; }
    }
}
