﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Models
{
    public enum State
    {
        Initial,
        Balance,
        Stake,
        Roll,
        DisplayMatrix,
        Calculate,
        Pay,
        DisplayResult,
        End,
    }
}
