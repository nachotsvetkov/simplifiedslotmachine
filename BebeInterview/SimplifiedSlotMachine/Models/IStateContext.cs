﻿using SimplifiedSlotMachine.Models.Entities;

namespace SimplifiedSlotMachine.Models
{
    public interface IStateContext
    {
        Player Player { get; set; }

        Game Game { get; set; }
    }
}