﻿using System;
using System.Collections.Generic;
using System.Text;
using SimplifiedSlotMachine.Models.Entities;

namespace SimplifiedSlotMachine.Models
{
    public class StateContext : IStateContext
    {
        public StateContext(Player player, Game game)
        {
            this.Player = player;
            this.Game = game;
        }

        public Player Player { get; set; }

        public Game Game { get; set;  }
    }
}
