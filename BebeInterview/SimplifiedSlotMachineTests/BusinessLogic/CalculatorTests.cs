﻿using FluentAssertions;
using NUnit.Framework;
using SimplifiedSlotMachine.BusinessLogic;
using SimplifiedSlotMachine.Models;

namespace SimplifiedSlotMachineTests.BusinessLogic
{
    public class CalculatorTests
    {
        public ICalculator Calculator { get; private set; }

        [SetUp]
        public void Setup()
        {
            this.Calculator = new Calculator();
        }

        [Test]
        public void BuildTest()
        {
            this.Calculator.Should().NotBeNull();
        }

        [Test]
        public void SingleRowValidTest()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[1, 3]
                {
                    { 
                        new Slot { DisplaySymbol = '*', Coefficient = 0.0m, IsWildCard = true },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                    }
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(0.8m);
        }

        [Test]
        public void SingleRowExamplePineappleAndWildCardsTest()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[1, 3]
                {
                    {
                        new Slot { DisplaySymbol = '*', Coefficient = 0.0m, IsWildCard = true},
                        new Slot { DisplaySymbol = 'P', Coefficient = 0.8m },
                        new Slot { DisplaySymbol = '*', Coefficient = 0.0m, IsWildCard = true},
                    }
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(0.8m);
        }

        [Test]
        public void SingleRowExample2Test()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[1, 3]
                {
                    {
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                    }
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(1.2m);
        }

        [Test]
        public void SingleRowExampleBRowTest()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[1, 3]
                {
                    {
                        new Slot { DisplaySymbol = 'B', Coefficient = 0.6m },
                        new Slot { DisplaySymbol = 'B', Coefficient = 0.6m },
                        new Slot { DisplaySymbol = 'B', Coefficient = 0.6m },
                    }
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(1.8m);
        }

        [Test]
        public void SingleRowExample3Test()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[1, 3]
                {
                    {
                        new Slot { DisplaySymbol = 'P', Coefficient = 0.8m },
                        new Slot { DisplaySymbol = 'P', Coefficient = 0.8m },
                        new Slot { DisplaySymbol = 'P', Coefficient = 0.8m },
                    }
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(2.4m);
        }

        [Test]
        public void SingleRowExample4Test()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[1, 3]
                {
                    {
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'B', Coefficient = 0.6m },
                        new Slot { DisplaySymbol = 'P', Coefficient = 0.8m },
                    }
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(0.0m);
        }

        [Test]
        public void SingleRowExample5Test()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[1, 3]
                {
                    {
                        new Slot { DisplaySymbol = '*', Coefficient = 0.0m, IsWildCard = true },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'B', Coefficient = 0.6m },
                    }
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(0.0m);
        }

        [Test]
        public void ExampleValidTest()
        {
            var matrix = new SlotMatrix
            {
                Cells = new Slot[4, 3]
                {
                    {
                        new Slot { DisplaySymbol = 'B', Coefficient = 0.6m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                    },
                    {
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                    },
                    {
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = '*', Coefficient = 0.0m, IsWildCard = true},
                        new Slot { DisplaySymbol = 'B', Coefficient = 0.6m },
                    },
                    {
                        new Slot { DisplaySymbol = '*', Coefficient = 0.0m, IsWildCard = true},
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                        new Slot { DisplaySymbol = 'A', Coefficient = 0.4m },
                    },
                }
            };

            var winCoef = this.Calculator.GetWinningCoef(matrix);
            winCoef.Should().Be(2m);
        }
    }
}