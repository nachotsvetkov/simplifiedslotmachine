﻿using Bogus;
using FluentAssertions;
using NUnit.Framework;
using SimplifiedSlotMachine.BusinessLogic;
using SimplifiedSlotMachine.Models;
using System;

namespace SimplifiedSlotMachineTests.BusinessLogic
{
    public class RollEngineTests
    {
        public RollEngine RollEngine { get; private set; }

        [SetUp]
        public void Setup()
        {
            this.RollEngine = new RollEngine(new Slot[]
            {
                new Slot { DisplaySymbol = 'A', Coefficient = 0.4m, ShowRatePercentage = 45 },
                new Slot { DisplaySymbol = 'B', Coefficient = 0.6m, ShowRatePercentage = 35 },
                new Slot { DisplaySymbol = 'P', Coefficient = 0.8m, ShowRatePercentage = 15 },
                new Slot { DisplaySymbol = '*', Coefficient = 0.0m, ShowRatePercentage = 5, IsWildCard = true },
            });
        }

        [Test]
        public void BuildTest()
        {
            this.RollEngine.Should().NotBeNull();
        }

        [Test]
        public void RollOutputTest()
        {
            var roll = this.RollEngine.Roll();
            roll.Should().NotBeNull();
            roll.Cells.GetLength(0).Should().Be(4);
            roll.Cells.GetLength(1).Should().Be(3);
        }

        [Test]
        public void PercentageValidationValidTest()
        {
            Action act = () => new RollEngine(new Slot[]
            {
                new Slot { DisplaySymbol = 'A', Coefficient = 0.4m, ShowRatePercentage = 45 },
                new Slot { DisplaySymbol = 'B', Coefficient = 0.6m, ShowRatePercentage = 35 },
                new Slot { DisplaySymbol = 'P', Coefficient = 0.8m, ShowRatePercentage = 15 },
                new Slot { DisplaySymbol = '*', Coefficient = 0.0m, ShowRatePercentage = 5, IsWildCard = true },
            });

            act.Should().NotThrow();
        }

        [Test]
        public void PercentageValidationLessThan100Test()
        {
            Action act = () => new RollEngine(new Slot[]
            {
                new Slot { DisplaySymbol = 'A', Coefficient = 0.4m, ShowRatePercentage = 45 },
                new Slot { DisplaySymbol = 'B', Coefficient = 0.6m, ShowRatePercentage = 35 },
                new Slot { DisplaySymbol = 'P', Coefficient = 0.8m, ShowRatePercentage = 15 },
            });

            act.Should().Throw<ArgumentException>();
        }

        [Test]
        public void PercentageValidationMoreThan100Test()
        {
            Action act = () => new RollEngine(new Slot[]
            {
                new Slot { DisplaySymbol = 'A', Coefficient = 0.4m, ShowRatePercentage = 45 },
                new Slot { DisplaySymbol = 'B', Coefficient = 0.6m, ShowRatePercentage = 35 },
                new Slot { DisplaySymbol = 'P', Coefficient = 0.8m, ShowRatePercentage = 15 },
                new Slot { DisplaySymbol = '*', Coefficient = 0.0m, ShowRatePercentage = 5, IsWildCard = true },
                new Slot { DisplaySymbol = '*', Coefficient = 0.0m, ShowRatePercentage = 5, IsWildCard = true },
            });

            act.Should().Throw<ArgumentException>();
        }

        [Test]
        public void RollEngineNoSlotsTest()
        {
            Action act = () => new RollEngine(new Slot[] { });

            act.Should().Throw<ArgumentException>();
        }

        [Test]
        public void RollEngine100SlotsTest()
        {
            var slotArray = new Faker<Slot>()
                .RuleFor(s => s.DisplaySymbol, f => f.PickRandom(new char[] { 'A', 'B', 'P', '*', }))
                .RuleFor(s => s.Coefficient, f => f.Random.Decimal())
                .RuleFor(s => s.ShowRatePercentage, f => 1)
                .Generate(100)
                .ToArray();

            Action act = () => new RollEngine(slotArray);

            act.Should().NotThrow();
        }

        [Test]
        public void RollEngine1SlotTest()
        {
            var slotArray = new Faker<Slot>()
                .RuleFor(s => s.DisplaySymbol, f => f.PickRandom(new char[] { 'A', 'B', 'P', '*', }))
                .RuleFor(s => s.Coefficient, f => f.Random.Decimal())
                .RuleFor(s => s.ShowRatePercentage, f => 100)
                .Generate(1)
                .ToArray();

            Action act = () => new RollEngine(slotArray);

            act.Should().NotThrow();
        }

        [Test]
        public void Roll100SlotsTest()
        {
            var slotArray = new Faker<Slot>()
                .RuleFor(s => s.DisplaySymbol, f => f.PickRandom(new char[] { 'A', 'B', 'P', '*', }))
                .RuleFor(s => s.Coefficient, f => f.Random.Decimal())
                .RuleFor(s => s.ShowRatePercentage, f => 1)
                .Generate(100)
                .ToArray();

            var rollEngine = new RollEngine(slotArray);
            Action act = () => rollEngine.Roll();

            act.Should().NotThrow();
        }

        [Test]
        public void Roll1SlotTest()
        {
            var slotArray = new Faker<Slot>()
                .RuleFor(s => s.DisplaySymbol, f => f.PickRandom(new char[] { 'A', 'B', 'P', '*', }))
                .RuleFor(s => s.Coefficient, f => f.Random.Decimal())
                .RuleFor(s => s.ShowRatePercentage, f => 100)
                .Generate(1)
                .ToArray();

            var rollEngine = new RollEngine(slotArray);
            Action act = () => rollEngine.Roll();

            act.Should().NotThrow();
        }
    }
}
