﻿using FluentAssertions;
using NUnit.Framework;
using SimplifiedSlotMachine.BusinessLogic;
using SimplifiedSlotMachine.BusinessLogic.StateHandlers;
using SimplifiedSlotMachine.Models;
using SimplifiedSlotMachine.Models.Entities;
using System.Collections.Generic;

namespace SimplifiedSlotMachineTests.BusinessLogic
{
    public class StateEngineTests
    {
        public IStateMachine StateMachine { get; private set; }

        [SetUp]
        public void Setup()
        {
            var context = new StateContext(new Player(), new Game());
            var provider = new ConsoleIOProvider();
            var rollEngine = new RollEngine(new Slot[] { new Slot { Coefficient = 1, DisplaySymbol = 'T', ShowRatePercentage = 100 } });
            var calculator = new Calculator();

            var stateHandlers = new Dictionary<State, IStateHandler>
            {
                { State.Initial, new InitialStateHandler(context) },
                { State.Balance, new BalanceStateHandler(context, provider) },
                { State.Stake, new StakeStateHandler(context, provider) },
                { State.Roll, new RollStateHandler(context, rollEngine) },
                { State.DisplayMatrix, new DisplayMatrixStateHandler(context, provider) },
                { State.Calculate, new CalculateStateHandler(context, calculator) },
                { State.Pay, new PayStateHandler(context) },
                { State.DisplayResult, new DisplayResultStateHandler(context, provider) },
                { State.End, new EndStateHandler(provider) },
            };

            this.StateMachine = new StateMachine(stateHandlers, context);
        }

        [Test]

        public void BuildTest()
        {
            this.StateMachine.Should().NotBeNull();
        }
    }
}
