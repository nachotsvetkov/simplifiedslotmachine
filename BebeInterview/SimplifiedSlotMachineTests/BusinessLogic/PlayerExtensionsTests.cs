﻿using FluentAssertions;
using NUnit.Framework;
using SimplifiedSlotMachine.BusinessLogic;
using SimplifiedSlotMachine.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachineTests.BusinessLogic
{
    public class PlayerExtensionsTests
    {
        public Player Player { get; private set; }

        [SetUp]
        public void Setup()
        {
            this.Player = new Player();
        }

        [Test]
        public void CanPlayTest()
        {
            this.Player.Balance = 1;
            this.Player.CanPlay().Should().BeTrue();
        }

        [Test]
        public void CantPlayTest()
        {
            this.Player.Balance = 0;
            this.Player.CanPlay().Should().BeFalse();
        }

        [Test]
        public void CantPlayInvalidTest()
        {
            this.Player.Balance = -1;
            this.Player.CanPlay().Should().BeFalse();
        }
    }
}
